# My Money Aurelia

Projeto de front-end para disciplina de Front-end Avançado.

Esse projeto representa a interface com o usuário criado (em 1 único dia e na raça) escrito utilizando o framework Aurelia (http://aurelia.io)

## Executando o projeto

### Baixando e executando serviço de Back-end localmente
Para que esse projeto funcione ele precisa que o serviço de Back-end esteja executando para realizar as chamadas HTTP Rest. O código do projeto de Back-end está disponível em:

https://gitlab.com/ufg-pos-full-stack-web-mobile/programacao-frontend-avancada/my-money-api


O JAR executável pode ser baixado no seguinte link:

https://goo.gl/euaeAN

Para executar o projeto, basta digitar na linha de comando: 

```
$ java -jar my-money-api-1.1.0.jar
```

É preciso ter o Java JRE na versão 8 instalada na máquina.

### Baixando e executando o front-end

Instale o NPM ou o YARN na sua máquina.

Faça o clone desse projeto, vá na linha de comando, no diretório que o projeto foi clonado e digite:

NPM  
``` 
$ npm install
$ Npm run start
```
ou

YARN
``` 
$ yarn install
$ yarn start
```

Enjoy it;
