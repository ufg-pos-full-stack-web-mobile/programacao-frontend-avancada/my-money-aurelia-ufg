import { LancamentoFilter } from './dto/lancamentoFilter';
import { ContaConnector } from './../conta/connectors/conta-connector';
import { ContaDTO } from './../conta/dto/contaDto';
import { LancamentoConnector } from './connector/lancamentoConnector';
import { LancamentoDTO } from './dto/lancamentoDto';

export class LancamentosList {    
  private _message: string
  private _lancamentos: LancamentoDTO[]
  private _lancamentoConnector: LancamentoConnector
  private _contaConnector: ContaConnector

  private _diaInicial: string
  private _diaFinal: string
  private _valorInicial: number
  private _valorFinal: number
  private _contas: ContaDTO[]
  private _conta: ContaDTO
  
  constructor() {
    this._message = 'Lançamentos'
    this._lancamentoConnector = new LancamentoConnector()
    this._contaConnector = new ContaConnector()

    this.loadLancamentos();
    this.loadContas();
  }

  filter() {
    let filter: LancamentoFilter = new LancamentoFilter()
    filter.diaInicial = new Date(this._diaInicial).getTime()
    filter.diaFinal = new Date(this._diaFinal).getTime()
    filter.valorInicial = this._valorInicial
    filter.valorFinal = this._valorFinal
    if (this._conta !== null) {
      filter.idConta = this._conta.codigo
    }

    this._lancamentoConnector.filter(filter)
        .then(response => {
          this._lancamentos = JSON.parse(JSON.stringify( response.content ))
        })
        .catch(() => {
          this.loadLancamentos()
        })

  }

  get diaInicial(): string {
    return this._diaInicial
  }
  set diaInicial(diaInicial: string) {
    this._diaInicial = diaInicial
  }
  get diaFinal(): string {
    return this._diaFinal
  }
  set diaFinal(diaFinal: string) {
    this._diaFinal = diaFinal
  }
  get valorInicial(): number {
    return this._valorInicial
  }
  set valorInicial(valorInicial: number) {
    this._valorInicial = valorInicial
  }
  get valorFinal(): number {
    return this._valorFinal
  }
  set valorFinal(valorFinal: number) {
    this._valorFinal = valorFinal
  }
  get contas(): ContaDTO[] {
    return this._contas
  }
  get conta(): ContaDTO {
    return this._conta
  }
  set conta(conta: ContaDTO) {
    this._conta = conta
  }

  get message(): string {
    return this._message
  }

  get lancamentos(): LancamentoDTO[] {
    return this._lancamentos
  }

  private loadLancamentos(): void {
    this._lancamentoConnector.getAll()
        .then(response => {
          this._lancamentos = JSON.parse(JSON.stringify( response.content ))
          this._lancamentos.forEach(lancamento => {
            let data = new Date(lancamento.dia)
            lancamento.data = data.getDay() + '/' + data.getMonth() + '/' + data.getFullYear()
          })
        })
        .catch(() => {
          this._lancamentos = []
        })
  }

  private loadContas(): void {
    this._contaConnector.getAll()
        .then(response => {
          this._contas = JSON.parse(JSON.stringify( response.content ));
        })
        .catch(() => {
          this._contas = []
        })
  }
}
