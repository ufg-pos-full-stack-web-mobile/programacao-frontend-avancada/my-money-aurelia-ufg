import { LancamentoFilter } from './../dto/lancamentoFilter';
import { NewLancamentoDTO } from './../dto/newLancamentoDto';
import { HttpClient, HttpResponseMessage } from 'aurelia-http-client';

export class LancamentoConnector {
  
  private _http: HttpClient

  constructor() {
    this._http = new HttpClient().configure(x => {
      x.withBaseUrl('http://localhost:8080')
      x.withHeader('Content-Type', 'application/json')
      x.withHeader('Accept', '*/*')
    })
  }

  getAll(): Promise<HttpResponseMessage> {
    return this._http.get('/lancamentos')
  }

  save(lancamento: NewLancamentoDTO): Promise<HttpResponseMessage> {
    return this._http.post('/lancamentos', lancamento)
  }

  filter(lancamentoFilter: LancamentoFilter): Promise<HttpResponseMessage> {
    return this._http.post('/lancamentos/filtro', lancamentoFilter)
  }

}
