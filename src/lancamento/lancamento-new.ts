import { NewLancamentoDTO } from './dto/newLancamentoDto';
import { ContaConnector } from './../conta/connectors/conta-connector';
import { ContaDTO } from './../conta/dto/contaDto';
import { LancamentoConnector } from './connector/lancamentoConnector';
import { LancamentoDTO } from './dto/lancamentoDto';

export class LancamentoNew {    
  private _message: string;
  private _lancamento: NewLancamentoDTO
  private _lancamentoConnector: LancamentoConnector
  private _contasConnector: ContaConnector

  private _dia: string;
  private _tiposLancamentos: string[]
  private _tipo: string
  private _valor: number
  private _contas: ContaDTO[]
  private _conta: ContaDTO

  
  constructor() {
    this._message = 'Novo lançamento';
    this._lancamentoConnector = new LancamentoConnector()
    this._contasConnector = new ContaConnector()

    this.loadContas()
    this.inicializarTiposLancamento()

  }

  save() {
    this._lancamento = new NewLancamentoDTO()
    this._lancamento.dia = new Date(this._dia).getTime()
    this._lancamento.tipo = this._tipo
    this._lancamento.valor = this._valor
    this._lancamento.conta = this._conta.codigo

    this._lancamentoConnector.save(this._lancamento)
        .then(() => {
          this._lancamento = new NewLancamentoDTO()
          this._dia = ''
          this._tipo = ''
          this._valor = 0
          this._conta = undefined
        })
  }

  get message(): string {
    return this._message
  }

  get dia(): string {
    return this._dia
  }
  set dia(dia: string) {
    this._dia = dia
  }
  get tiposLancamentos(): string[] {
    return this._tiposLancamentos
  }
  get tipo() {
    return this._tipo
  }
  set tipo(tipo: string) {
    this._tipo = tipo
  }
  get valor(): number {
    return this._valor
  }
  set valor(valor: number) {
    this._valor = valor
  }
  get contas(): ContaDTO[] {
    return this._contas
  }
  get conta(): ContaDTO {
    return this._conta
  }
  set conta(conta: ContaDTO) {
    this._conta = conta
  }

  private loadContas(): void {
    this._contasConnector.getAll()
        .then(response => {
          this._contas = JSON.parse(JSON.stringify( response.content ))
        })
  }

  private inicializarTiposLancamento() {
    this._tiposLancamentos = ['ENTRADA', 'SAIDA']
  }
}
