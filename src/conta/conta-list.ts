import { ContaConnector } from './connectors/conta-connector';
import {ContaDTO} from './dto/contaDto'

export class ContaList {
  private _message: string;
  private _contasDto: ContaDTO[];
  private _contaConnector: ContaConnector;
  
  constructor() {
    this._message = 'Contas';
    this._contaConnector = new ContaConnector();

    this.loadAll()
  }

  excluir(conta: ContaDTO) {
    return this._contaConnector.delete(conta.codigo)
    .then(() => {
      this.loadAll();
    })
  }

  get message(): string {
    return this._message;
  }

  get contas(): ContaDTO[] {
    return this._contasDto;
  }

  private loadAll() {
    this._contaConnector.getAll()
        .then(response => {
          this._contasDto = JSON.parse(JSON.stringify( response.content ))
        }).catch(() => {
          this._contasDto = []
        });
  }
}
