import {HttpClient, HttpResponseMessage} from 'aurelia-http-client'
import { ContaDTO } from '../dto/contaDto';

export class ContaConnector {

  private _httpClient: HttpClient

  constructor() {
    this._httpClient = new HttpClient().configure(x => {
      x.withBaseUrl('http://localhost:8080')
      x.withHeader('Content-Type', 'application/json')
      x.withHeader('Accept', '*/*')
    });    
  }

  getAll(): Promise<HttpResponseMessage> {
    return this._httpClient.get('/contas')
  }

  save(contaDto: ContaDTO): Promise<HttpResponseMessage> {
    return this._httpClient.post('/contas', contaDto)
  }

  delete(codigo: number): Promise<HttpResponseMessage> {
    return this._httpClient.delete('/contas/' + codigo)
  }

}
