import { ContaConnector } from './connectors/conta-connector';
import { ContaDTO } from './dto/contaDto';

export class ContaNew {    
  private _message: string;
  private _conta: ContaDTO;
  private _contaConnector: ContaConnector;
  private _errorMessage: string;
  
  constructor() {
    this._message = 'Nova conta';
    this._contaConnector = new ContaConnector();
    this._conta = new ContaDTO();
  }

  save() {
      this._contaConnector.save(this._conta)
          .then(() => {this._conta = new ContaDTO()})
          .catch(response => {
            let json = JSON.parse(JSON.stringify( response.content ))
            this._errorMessage = json.mensagem
          })
  }

  get conta(): ContaDTO {
    return this._conta
  }

  get message(): string {
    return this._message
  }

  get msgError(): string {
    return this._errorMessage
  }

}
