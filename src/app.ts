export class App {
  private _message = 'Hello World!'
  private _router

  configureRouter(config, router) {
    this._router = router
    config.title = 'My-Money-Aurelia'
    config.map([
      { route: ['', 'home'],       name: 'home',       moduleId: 'home/index',    nav:true,     title: 'Home' },
      { route: 'contas',           name: 'contas',     moduleId: 'conta/conta-list', nav:true,  title: 'Conta'},
      { route: 'conta',            name: 'conta',      moduleId: 'conta/conta-new' },
      { route: 'lancamentos',      name: 'lancamentos', moduleId: 'lancamento/lancamentos-list', nav: true, title: 'Lancamento'},
      { route: 'lancamento',       name: 'lancamento', moduleId: 'lancamento/lancamento-new'}
    ])
  }

  get router() {
    return this._router
  }

}
